const { spawnSync } = require('child_process')

const exec = (serviceName, command) => {
   console.log(`Installing dependencies for [${serviceName}]`)
   console.log(`Folder: ./${serviceName} Command: ${command}`)
   spawnSync(command, [], { cwd: './' + serviceName, shell: true, stdio: 'inherit' })
}

module.exports = exec